#include <avr/io.h>
#include <avr/interrupt.h>
#include <Servo.h>
#include <Chrono.h>

#define SAMPLE_SIZE 1000
#define SENSOR_THRESH 105

const int buttonPin = 2;     // the number of the pushbutton pin
const int servoPin = 9;      // the number of the Servo pin
const int ledPin =  13;      // the number of the LED pin

Servo myservo;
Chrono mychrono;

volatile int doorState = 1;
volatile unsigned long last_millis=0;

int timeout=5000;
int analog;
long med;
int i=0;
char readIn=0;
int mouse=0;

void debounce() {
  if((long)(millis() - last_millis) >= 200) {
    button();
    last_millis = millis();
  }
}

//button actions
void button() {
  doorState=!doorState;
  if(doorState){
    Serial.println(1);
  }
  else{
    Serial.println(0);
  }
}
void setup() {
  pinMode(buttonPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(buttonPin), debounce, FALLING);

  pinMode(ledPin, OUTPUT);

  myservo.attach(servoPin);
  myservo.write(10);

  mychrono.stop();

  Serial.begin(9600);
  Serial.setTimeout(0);
}

void loop() {
//read sensor data
  if(i==SAMPLE_SIZE){
    //Serial.println(med/SAMPLE_SIZE);
    if(med/SAMPLE_SIZE<SENSOR_THRESH && mouse==0 && doorState==1){
      Serial.println(2);
      mychrono.restart();
      mouse=1;
    }
    med=0;
    i=0;
  }
  i++;
  analog=analogRead(0);
  med+=analog;
//read UART controls
  if(Serial.readBytes(&readIn, 1)!=0){
    if(readIn=='0'){
      doorState=0;
      mychrono.stop();
      mouse=0;
    }
    else if(readIn=='1'){
      doorState=1;
      mychrono.stop();
      mouse=0;
    }
    else if(readIn=='3'){
      if (timeout >= 1000){
        timeout-=1000;
      }
      Serial.println(timeout);
    }
    else if(readIn=='4'){
      timeout+=1000;
      Serial.println(timeout);
    }
  }
//verify if timeout after detecting mouse
  if(mychrono.hasPassed(timeout)){
    doorState=0;
    mychrono.restart();
    mychrono.stop();
    mouse=0;
    Serial.println(0);
  }
//open/close door
  if(doorState){
    digitalWrite(ledPin, HIGH);
    myservo.write(10);
  }else {
    digitalWrite(ledPin, LOW);
    myservo.write(170);
  }
}

