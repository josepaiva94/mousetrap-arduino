# MouseTrap - Arduino component

This project contains the code of the Arduino part of MouseTrap project. It is responsible for managing the motor of the door, read input from the sensor and inform the Raspberry Pi of the state of the door and sensor detections. It must also execute orders received from Raspberry Pi. Arduino communicates with Raspberry Pi through serial communication.

## Getting Started

Clone the repo or download it.

## Requirements

Trap mounted and connected to Arduino and Raspberry Pi connected via UART (or USB) to Arduino. Check the image below.

![wired-schema.png](https://bitbucket.org/repo/5qqbM5K/images/910067964-wired-schema.png)

## Deployment

Load trap.ino to your Arduino.

## Issue Reporting

If you have found a bug or if you have a feature request, please report them at this repository issues section. Please do not report security vulnerabilities on the public issues section, instead send an email to up201207840 [at] fc.up.pt

## Author

[Pedro Moreno](#)

## License

This project is licensed under the MIT license. See the [LICENSE](LICENSE.txt) file for more info.